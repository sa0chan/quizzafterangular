package com.zenika.academy.quiz.services.factory;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.OpenQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.domain.questions.TrueFalseQuestion;
import com.zenika.academy.quiz.services.IdGenerator;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class MovieQuestionFactory {

    private TmdbClient tmdb;
    private RandomMovieElementsGenerator randomMovieElementsGenerator;
    private Random random;
    private IdGenerator idGenerator;

    public MovieQuestionFactory(TmdbClient tmdb, RandomMovieElementsGenerator randomMovieElementsGenerator, Random random, IdGenerator idGenerator) {
        this.tmdb = tmdb;
        this.randomMovieElementsGenerator = randomMovieElementsGenerator;
        this.random = random;
        this.idGenerator = idGenerator;
    }

    public Optional<Question> createMovieQuestion(String movieTitle, QuestionLevel level) {
        return this.tmdb.getMovie(movieTitle)
                .map(movie -> this.buildMovieQuestion(movie, level));
    }

    public Optional<Question> createCharacterQuestion(String movietitle, QuestionLevel level) {
        return this.tmdb.getMovie(movietitle)
                .map(movie -> this.buildCharacterQuestion(movie, level));
    }

    private Question buildMovieQuestion(MovieInfo movie, QuestionLevel level) {
        String actors = movie.cast.stream()
                .map(c -> c.actorName)
                .limit(3)
                .collect(Collectors.joining(", "));
        String text = "Quel film sorti en " + movie.year + " contient des personnages joué par les acteurs suivants : " + actors + " ?";
        switch (level) {
            case EASY:
                List<String> badAnswers = this.randomMovieElementsGenerator.getRandomMovieNames(2, movie.title);
                return new MultipleChoiceQuestion(this.idGenerator.generateNewId(), text, badAnswers, movie.title, this.random);
            case MEDIUM:
            case HARD:
                return new OpenQuestion(this.idGenerator.generateNewId(), text, movie.title);
        }
        throw new IllegalArgumentException("Question level must not be null");
    }

    private Question buildCharacterQuestion(MovieInfo movie, QuestionLevel level) {
        MovieInfo.Character correctAnswerCharacter = movie.cast.get(0);
        MovieInfo.Character badAnswerCharacter1 = movie.cast.get(1);
        MovieInfo.Character badAnswerCharacter2 = movie.cast.get(2);
        switch (level) {
            case EASY:
                String textTrueFalse = "Le personnage joué par " + correctAnswerCharacter.actorName + " dans le film " + movie.title + " s'appelle " + correctAnswerCharacter.characterName;
                return new TrueFalseQuestion(this.idGenerator.generateNewId(), textTrueFalse, true); // TODO use randomization to have false questions
            case MEDIUM:
                String textQCM = "Comment s'appelle le personnage joué par " + correctAnswerCharacter.actorName + " dans le film " + movie.title + " ?";
                return new MultipleChoiceQuestion(this.idGenerator.generateNewId(), textQCM, List.of(badAnswerCharacter1.characterName, badAnswerCharacter2.characterName), correctAnswerCharacter.characterName, this.random);
            case HARD:
                return new OpenQuestion(this.idGenerator.generateNewId(), "Comment s'appelle le personnage joué par " + correctAnswerCharacter.actorName + " dans le film " + movie.title + " ?", correctAnswerCharacter.characterName);
        }
        throw new IllegalArgumentException("Question level must not be null");
    }

    private String getRandomMovieName() {
        return this.tmdb.getRandomPopularMovie().map(m -> m.title).orElseThrow();
    }
}
