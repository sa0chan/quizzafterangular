package com.zenika.academy.quiz.services;

import com.zenika.academy.quiz.controllers.representation.NewQuestionRepresentation;
import com.zenika.academy.quiz.domain.questions.*;
import com.zenika.academy.quiz.repositories.QuestionRepository;
import com.zenika.academy.quiz.repositories.QuizRepository;
import com.zenika.academy.quiz.services.exceptions.NoMovieException;
import com.zenika.academy.quiz.services.factory.MovieQuestionFactory;
import com.zenika.academy.quiz.services.factory.QuestionLevel;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class QuestionService {
    private QuestionRepository questionRepository;
    private MovieQuestionFactory movieQuestionFactory;
    private IdGenerator idGenerator;
    private QuizRepository quizRepository;

    public QuestionService(QuestionRepository questionRepository, MovieQuestionFactory movieQuestionFactory, IdGenerator idGenerator, QuizRepository quizRepository) {
        this.questionRepository = questionRepository;
        this.movieQuestionFactory = movieQuestionFactory;
        this.idGenerator = idGenerator;
        this.quizRepository = quizRepository;
    }

    public List<Question> getAllQuestions() {
        return this.questionRepository.getAll();
    }

    public Question createMovieQuestion(String movie) {
        final Question createdQuestion = this.movieQuestionFactory
                .createMovieQuestion(movie, QuestionLevel.HARD)
                .orElseThrow(() -> new NoMovieException("Le film " + movie + " ne semble pas exister"));
        this.questionRepository.save(createdQuestion);
        return createdQuestion;
    }

    public Optional<Question> getOneQuestion(String id) {
        return this.questionRepository.getOne(id);
    }

    public Optional<AnswerResult> tryAnswerById(String id, String answer) {
        return this.questionRepository.getOne(id)
                .map(q -> q.tryAnswer(answer));
    }

    public Question createQuestion(NewQuestionRepresentation body) {
        // Determine the type of question to create
        if(StringUtils.isEmpty(body.getText())) {
            throw new IllegalArgumentException("Missing question text");
        }

        final Question createdQuestion;
        if(body.getTrueFalseAnswer() != null) {
            createdQuestion = new TrueFalseQuestion(this.idGenerator.generateNewId(), body.getText(), body.getTrueFalseAnswer());
        }
        else if (body.getIncorrectSuggestions() != null && !StringUtils.isEmpty(body.getCorrectAnswer())) {
            createdQuestion = new MultipleChoiceQuestion(this.idGenerator.generateNewId(), body.getText(), body.getIncorrectSuggestions(), body.getCorrectAnswer(), new Random());
        }
        else if (!StringUtils.isEmpty(body.getCorrectAnswer())) {
            createdQuestion = new OpenQuestion(this.idGenerator.generateNewId(), body.getText(), body.getCorrectAnswer());
        }
        else {
            throw new IllegalArgumentException("Could not determine the type of question to create");
        }

        if(this.quizRepository.getOne(body.getQuizId()).isPresent()) {
            createdQuestion.addToQuiz(body.getQuizId());
        }

        this.questionRepository.save(createdQuestion);
        return createdQuestion;
    }

}
