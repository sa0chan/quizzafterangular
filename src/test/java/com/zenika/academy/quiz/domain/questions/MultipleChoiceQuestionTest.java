package com.zenika.academy.quiz.domain.questions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;

import static com.zenika.academy.quiz.domain.questions.AnswerResult.CORRECT;
import static com.zenika.academy.quiz.domain.questions.AnswerResult.INCORRECT;
import static java.lang.System.lineSeparator;

class MultipleChoiceQuestionTest {

    private final Random seededRandom = new Random(1);

    @Test
    void displayableTextShouldIncludeSuggestions() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new MultipleChoiceQuestion(
                id,
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals("Qui a marché sur la lune ?" + lineSeparator() +
                "  1) Lance Armstrong" + lineSeparator() +
                "  2) Neil Armstrong" + lineSeparator() +
                "  3) Louis Armstrong" + lineSeparator(), q.getDisplayableText());
    }

    @Test
    void correctTextAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new MultipleChoiceQuestion(
                id,
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(CORRECT, q.tryAnswer("Neil Armstrong"));
    }

    @Test
    void correctIndexAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new MultipleChoiceQuestion(
                id,
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(CORRECT, q.tryAnswer("2"));
    }

    @Test
    void incorrectIndexAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new MultipleChoiceQuestion(
                id,
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(INCORRECT, q.tryAnswer("3"));
    }

    @Test
    void incorrectTextAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new MultipleChoiceQuestion(
                id,
                "Qui a marché sur la lune ?",
                List.of("Louis Armstrong", "Lance Armstrong"),
                "Neil Armstrong",
                this.seededRandom
        );

        Assertions.assertEquals(INCORRECT, q.tryAnswer("Louis Armstrong"));
    }
}