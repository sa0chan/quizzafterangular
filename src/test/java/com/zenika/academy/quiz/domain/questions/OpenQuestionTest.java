package com.zenika.academy.quiz.domain.questions;

import com.zenika.academy.quiz.services.IdGenerator;
import org.junit.jupiter.api.Test;

import static com.zenika.academy.quiz.domain.questions.AnswerResult.CORRECT;
import static com.zenika.academy.quiz.domain.questions.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.assertEquals;

class OpenQuestionTest {

    @Test
    void getDisplayableText() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new OpenQuestion(id, "Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals("Qui marche de travers ?", q.getDisplayableText());
    }

    @Test
    void tryCorrectAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new OpenQuestion(id, "Qui marche de travers ?", "C'est le crabe tout vert");
        assertEquals(CORRECT, q.tryAnswer("C'est le crabe tout vert"));
    }

    @Test
    void tryIncorrectAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new OpenQuestion(id, "Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals(INCORRECT, q.tryAnswer("Un homme en état d'ébriété"));
    }

    @Test
    void tryAlmostCorrectAnswer() {
        String id = "954de1b9-e7e8-403e-8ddc-ac26033195bf";
        Question q = new OpenQuestion(id, "Qui marche de travers ?", "C'est le crabe tout vert");

        assertEquals(INCORRECT, q.tryAnswer("C'est le crbe tout vart"));
    }
}