package com.zenika.academy.quiz.controllers.representation;

public class UserAnswerRepresentation {
    private String answer;

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
