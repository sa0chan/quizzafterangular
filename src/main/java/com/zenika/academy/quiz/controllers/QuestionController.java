package com.zenika.academy.quiz.controllers;

import com.zenika.academy.quiz.controllers.representation.*;
import com.zenika.academy.quiz.domain.questions.AnswerResult;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private QuestionService questionService;
    private QuestionRepresentationMapper mapper;

    @Autowired
    public QuestionController(QuestionService questionService, QuestionRepresentationMapper mapper) {
        this.questionService = questionService;
        this.mapper = mapper;
    }

    @GetMapping
    List<DisplayableQuestionRepresentation> getAllQuestions() {
        return this.questionService.getAllQuestions().stream()
                .map(this.mapper::mapToDisplayableQuestion)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<DisplayableQuestionRepresentation> getOneQuestion(@PathVariable("id") String id) {
        Optional<Question> question = this.questionService.getOneQuestion(id);

        return question
                .map(this.mapper::mapToDisplayableQuestion)
                .map(ResponseEntity::ok) // Optional<ResponseEntity<Question>>
                .orElseGet(() -> ResponseEntity.notFound().build()); // ResponseEntity<Question>
    }

    @PostMapping
    DisplayableQuestionRepresentation createQuestion(@RequestParam(value = "movie", required = false) String movie, @RequestBody(required = false) NewQuestionRepresentation body) {
        final Question createdQuestion;
        if (movie != null && body == null) {
            createdQuestion = this.questionService.createMovieQuestion(movie);
        } else if (body != null && movie == null) {
            createdQuestion = this.questionService.createQuestion(body);
        } else {
            throw new IllegalArgumentException("Please provide either a body or a movie title (but not both)");
        }
        return this.mapper.mapToDisplayableQuestion(createdQuestion);
    }

    @PostMapping("/{id}/:tryAnswer")
    ResponseEntity<AnswerResultRepresentation> tryAnswer(@PathVariable("id") String id, @RequestBody UserAnswerRepresentation body) {
        return this.questionService.tryAnswerById(id, body.getAnswer())
                .map(this::createAnswerRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private AnswerResultRepresentation createAnswerRepresentation(AnswerResult answerResult) {
        final AnswerResultRepresentation result = new AnswerResultRepresentation();
        result.setResult(answerResult.toString());
        return result;
    }
}
