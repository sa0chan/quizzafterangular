package com.zenika.academy.quiz.controllers.representation;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import org.springframework.stereotype.Component;

@Component
public class QuestionRepresentationMapper {

    public DisplayableQuestionRepresentation mapToDisplayableQuestion(Question q) {
        DisplayableQuestionRepresentation result = new DisplayableQuestionRepresentation();
        result.setId(q.getId());

        if(q instanceof MultipleChoiceQuestion) {
            result.setDisplayableText(((MultipleChoiceQuestion) q).getText());
            result.setSuggestions(((MultipleChoiceQuestion) q).getSuggestions());
        }
        else{
            result.setDisplayableText(q.getDisplayableText());
        }
        return result;
    }
}
