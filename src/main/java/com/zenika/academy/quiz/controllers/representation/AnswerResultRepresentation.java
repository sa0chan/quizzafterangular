package com.zenika.academy.quiz.controllers.representation;

public class AnswerResultRepresentation {
    private String result;

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
