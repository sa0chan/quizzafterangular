
--création de table score
create table score(
	id_player int references player(id),
	score int
)

--création de table question
create table question(
	id serial,
	displayable_text varchar,
	correct_answer varchar,
	correct_answer_bool boolean,
	suggestion1 varchar,
	suggestion2 varchar,
	suggestion3 varchar,

	primary key(id)
)

--création de table quizz
create table quizz(
    id serial,
    name text,
    id_question int references question(id),
    primary key (id)
)

-- inserer question

insert into question (displayable_text, correct_answer,correct_answer_bool, suggestion1,suggestion2,suggestion3)
values
('Qui est le créateur de ce quizz formidable ?', 'Sarah',null,'Erwan','Riri','Marion'),
('Dans quelle ville ce trouve la tour eiffel ?', 'Paris',null,null,null,null),
('Y a t il une pomme de terre sur la tasse de Riri ?', null,'true',null,null,null)


-- selectioner all question

select * from question

--select * from une question

select * from question
where id = 1

--update question
update question
SET nom_colonne_1 = 'nouvelle valeur'
WHERE condition


--delete question par l'id

DELETE FROM question
WHERE id = 1


--inserer un joueur

insert into player (name)
values ('Sa0chan')