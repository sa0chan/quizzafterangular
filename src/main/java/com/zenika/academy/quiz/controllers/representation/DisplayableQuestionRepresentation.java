package com.zenika.academy.quiz.controllers.representation;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableQuestionRepresentation {
    private String displayableText;
    private List<String> suggestions;
    private String id;

    public String getDisplayableText() {
        return this.displayableText;
    }

    public void setDisplayableText(String displayableText) {
        this.displayableText = displayableText;
    }

    public List<String> getSuggestions() {
        return this.suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
