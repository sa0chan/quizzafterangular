package com.zenika.academy.quiz.controllers.representation;

public class NewQuizRepresentation {
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
