package com.zenika.academy.quiz.controllers;

import com.zenika.academy.quiz.controllers.representation.DisplayableQuestionRepresentation;
import com.zenika.academy.quiz.controllers.representation.NewQuizRepresentation;
import com.zenika.academy.quiz.controllers.representation.QuestionRepresentationMapper;
import com.zenika.academy.quiz.controllers.representation.QuizRepresentation;
import com.zenika.academy.quiz.domain.Quiz;
import com.zenika.academy.quiz.services.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/quizzes")
public class QuizController {

    private QuizService quizService;
    private QuestionRepresentationMapper questionRepresentationMapper;

    @Autowired
    public QuizController(QuizService quizService, QuestionRepresentationMapper questionRepresentationMapper) {
        this.quizService = quizService;
        this.questionRepresentationMapper = questionRepresentationMapper;
    }

    @GetMapping
    public List<QuizRepresentation> getAll() {
        return this.quizService.getAll().stream()
                .map(q -> new QuizRepresentation(q.getId(), q.getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}/questions")
    public List<DisplayableQuestionRepresentation> getQuestionsOfQuiz(@PathVariable("id") String quizId) {
        return this.quizService.getQuestionsOfQuiz(quizId).stream()
                .map(this.questionRepresentationMapper::mapToDisplayableQuestion)
                .collect(Collectors.toList());
    }

    @PostMapping
    public QuizRepresentation createQuiz(@RequestBody NewQuizRepresentation body) {
        final Quiz createdQuiz = this.quizService.createQuiz(body);
        return new QuizRepresentation(createdQuiz.getId(), createdQuiz.getName());
    }

}
