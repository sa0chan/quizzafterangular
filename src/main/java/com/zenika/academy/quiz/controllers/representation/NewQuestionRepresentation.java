package com.zenika.academy.quiz.controllers.representation;

import java.util.List;

public class NewQuestionRepresentation {
    private String text;
    private String quizId;
    private Boolean trueFalseAnswer;
    private String correctAnswer;
    private List<String> incorrectSuggestions;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getTrueFalseAnswer() {
        return this.trueFalseAnswer;
    }

    public void setTrueFalseAnswer(boolean trueFalseAnswer) {
        this.trueFalseAnswer = trueFalseAnswer;
    }

    public String getCorrectAnswer() {
        return this.correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public List<String> getIncorrectSuggestions() {
        return this.incorrectSuggestions;
    }

    public void setIncorrectSuggestions(List<String> incorrectSuggestions) {
        this.incorrectSuggestions = incorrectSuggestions;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }
}
