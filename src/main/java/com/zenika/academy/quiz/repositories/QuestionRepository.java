package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.OpenQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.domain.questions.TrueFalseQuestion;
import com.zenika.academy.quiz.services.IdGenerator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class QuestionRepository {

    private JdbcTemplate template;


    public QuestionRepository(JdbcTemplate template) {

        this.template = template;

    }

    public Optional<Question> getOne(String id) {

        final RowMapper<Question> rowMapper = (rs, rowNum) -> {

            if (rs.getString("suggestion1") == null && rs.getString("correct_answer") != null) {
                return new OpenQuestion(id, rs.getString("displayable_text"), rs.getString("correct_answer"));
            } else if (rs.getString("suggestion1") != null) {
                Random random = new Random();

                List<String> arraySuggestion = new ArrayList<>();
                arraySuggestion.add(rs.getString("suggestion1"));
                arraySuggestion.add(rs.getString("suggestion2"));
                arraySuggestion.add(rs.getString("suggestion3"));

                return new MultipleChoiceQuestion(id, rs.getString("displayable_text"), arraySuggestion, rs.getString("correct_answer"), random);

            } else {
                return new TrueFalseQuestion(id, rs.getString("displayable_text"), rs.getBoolean("correct_answer_bool"));
            }
        };
        return Optional.ofNullable(template.queryForObject("select * from question where id = ?", rowMapper, id));
    }


    public List<Question> getAll() {

        List<Question> questionList = new ArrayList<>();

        final RowMapper<Question> rowMapperList = (rs, rowNum) -> {

            if (rs.getString("suggestion1") == null && rs.getString("correct_answer") != null) {
                return new OpenQuestion(rs.getString("id"), rs.getString("displayable_text"), rs.getString("correct_answer"));
            } else if (rs.getString("suggestion1") != null) {
                Random random = new Random();

                List<String> arraySuggestion = new ArrayList<>();
                arraySuggestion.add(rs.getString("suggestion1"));
                arraySuggestion.add(rs.getString("suggestion2"));
                arraySuggestion.add(rs.getString("suggestion3"));

                return new MultipleChoiceQuestion(rs.getString("id"), rs.getString("displayable_text"), arraySuggestion, rs.getString("correct_answer"), random);

            } else {
                return new TrueFalseQuestion(rs.getString("id"), rs.getString("displayable_text"), rs.getBoolean("correct_answer_bool"));
            }
            //si on met un return ici c'est ce qu'il renvera en cas d'impossibilité de rentrer dans le if mais ici il y a un else en fin donc pas besoin
        };

        return List.copyOf(template.query("select * from question", rowMapperList));
    }

    public List<Question> getByQuizId(String quizId) {
        return this.getAll().stream()
                .filter(question -> Objects.equals(question.getQuizId(), quizId))
                .collect(Collectors.toList());
    }


    public void save(Question q) {
        String text;
        String goodAnswer;
        Boolean goodAnswerBool;
        List arraySugg;
        IdGenerator idGenerator = new IdGenerator();
        String id = q.getId();

        if (q instanceof OpenQuestion) {
            OpenQuestion questionOpen = (OpenQuestion) q;
            text = questionOpen.getDisplayableText();
            goodAnswer = questionOpen.getCorrectAnswer();

            template.update("insert into question " +
                    "values (?,?,?,NULL,NULL,NULL,NULL)", id, text, goodAnswer);

        } else if (q instanceof MultipleChoiceQuestion) {

            MultipleChoiceQuestion multipleChoiceQuestion = (MultipleChoiceQuestion) q;
            text = multipleChoiceQuestion.getDisplayableText();
            goodAnswer = multipleChoiceQuestion.getCorrectAnswer();
            arraySugg = multipleChoiceQuestion.getSuggestions();

            template.update("insert into question " +
                    "values (?,?,?,NULL,?,?,?)", id, text, goodAnswer, arraySugg.get(0), arraySugg.get(1), arraySugg.get(2));

        } else if (q instanceof TrueFalseQuestion) {
            TrueFalseQuestion trueFalseQuestion = (TrueFalseQuestion) q;
            text = trueFalseQuestion.getDisplayableText();
            goodAnswer = null;
            goodAnswerBool = trueFalseQuestion.getCorrectAnswer();
            template.update("insert into question " +
                    "values (?,?,?,?,NULL,NULL,NULL)", id, text, goodAnswer, goodAnswerBool);
        }
    }


}
