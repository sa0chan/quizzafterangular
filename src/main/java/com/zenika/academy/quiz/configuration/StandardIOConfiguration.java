package com.zenika.academy.quiz.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Scanner;

@Configuration
@Profile("!test")
public class StandardIOConfiguration {
    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }
}
