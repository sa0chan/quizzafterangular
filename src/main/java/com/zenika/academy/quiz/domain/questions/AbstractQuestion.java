package com.zenika.academy.quiz.domain.questions;

public abstract class AbstractQuestion implements Question {
    private String id;
    private String quizId;

    AbstractQuestion(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public void addToQuiz(String quizId) {
        this.quizId = quizId;
    }

    @Override
    public String getQuizId() {
        return this.quizId;
    }
}
