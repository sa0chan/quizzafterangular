package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.Quiz;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class QuizRepository {
    private Map<String, Quiz> quizzesById = new HashMap<>();

    public List<Quiz> getAll() {
        return List.copyOf(this.quizzesById.values());
    }

    public void save(Quiz q) {
        this.quizzesById.put(q.getId(), q);
    }

    public Optional<Quiz> getOne(String id) {
        return Optional.ofNullable(this.quizzesById.get(id));
    }
}
