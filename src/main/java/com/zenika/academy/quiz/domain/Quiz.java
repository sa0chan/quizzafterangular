package com.zenika.academy.quiz.domain;

public class Quiz {
    private String id;
    private String name;

    public Quiz(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}
